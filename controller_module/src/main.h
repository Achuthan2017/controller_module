/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
    Revision Information:
        File name: main.h
        Version:   v0.0
        Date:      2020-05-19
    ============================================================================
    Dependency Graph
    |-- <Adafruit MCP23017 Arduino Library> 1.0.6
    |   |-- <Wire> 1.0
    |-- <Adafruit INA219> 1.0.9
    |   |-- <Adafruit NeoPixel> 1.4.0
    |   |-- <Adafruit GFX Library> 1.8.3
    |   |   |-- <Adafruit BusIO> 1.2.4
    |   |   |   |-- <SPI> 1.0
    |   |   |   |-- <Wire> 1.0
    |   |   |-- <SPI> 1.0
    |   |   |-- <Wire> 1.0
    |   |-- <Adafruit SSD1306> 2.2.1
    |   |   |-- <Adafruit GFX Library> 1.8.3
    |   |   |   |-- <Adafruit BusIO> 1.2.4
    |   |   |   |   |-- <SPI> 1.0
    |   |   |   |   |-- <Wire> 1.0
    |   |   |   |-- <SPI> 1.0
    |   |   |   |-- <Wire> 1.0
    |   |   |-- <SPI> 1.0
    |   |   |-- <Wire> 1.0
    |   |-- <Adafruit BusIO> 1.2.4
    |   |   |-- <SPI> 1.0
    |   |   |-- <Wire> 1.0
    |   |-- <Wire> 1.0
    |-- <DHT sensor library> 1.3.9
    |   |-- <Adafruit Unified Sensor> 1.1.2
    |   |   |-- <Adafruit ADXL343> 1.3.0
    |   |   |   |-- <Adafruit BusIO> 1.2.4
    |   |   |   |   |-- <SPI> 1.0
    |   |   |   |   |-- <Wire> 1.0
    |   |   |   |-- <Wire> 1.0
    |-- <EEPROM> 2.0
    |-- <Servo> 1.1.6
    |-- <Wire> 1.0
 */

/*
** ==========================================================================
**                        INCLUDE STATEMENTS
** ==========================================================================
*/

#ifndef MAIN_H
#define	MAIN_H

#include <Arduino.h>
#include <Wire.h>
#include <Servo.h>
#include <EEPROM.h>
#include <DHT.h>
#include <Adafruit_INA219.h>
#include "Adafruit_MCP23017.h"

#endif	/* MAIN_H */

